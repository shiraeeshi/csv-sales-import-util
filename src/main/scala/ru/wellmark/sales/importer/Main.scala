package ru.wellmark.sales.importer

import scala.io.Source
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import slick.jdbc.PostgresProfile.api._
import com.github.tototoshi.slick.PostgresJodaSupport._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import ru.wellmark.sales.importer.data.{GoodsEntry, GoodsEntryDAO, Shop, ShopDAO}
import ru.wellmark.sales.importer.services.{CGoodMatcher, GoodMatchService, GoodMatcher}

import scala.concurrent.Future


object Main extends App {

  if (args.length != 2) {
    System.err.println("Usage: Main <filename> <comma-delimited-inns>")
    System.exit(1)
  }

  import Utils._
  import Schemas._

  val filename = args(0)
  println(s"filename is $filename")

  val inns: Array[Long] =
    args(1)
      .split(",")
      .map(_.toLong)

  val pattern = {
    val quotesOrNot = """("(?>(?>""|[^"]+)+)"(?!")|[^,]+)"""
		val num = raw"(\d+)"
		val noDelimiter = "([^,]+)"
		val txt = raw"(\w+)"
		val floatingPointNum = raw"(\d+\.?\d*)"
    s"""(?U)$quotesOrNot,$num,$quotesOrNot,$txt,$noDelimiter,$num,$quotesOrNot,$quotesOrNot,$num,$floatingPointNum,$noDelimiter""".r
  }

  using(Database.forConfig("db")) { db =>

    val goodsFoundByInns: Future[Seq[GoodsEntry]] = db.run(GoodsEntryDAO.findByInns(inns))
    val goodMatchServiceFuture = goodsFoundByInns.map { goods =>
      new GoodMatchService {
        override val matcher: GoodMatcher = new CGoodMatcher
        override val goodById: Map[Long, GoodsEntry] =
          goods
            .map(g => (g.id.get, g))
            .toMap
      }
    }

    using(Source.fromFile(filename)) { source =>
      val futureUpsertResults = for (line <- source.getLines().drop(1)) yield {
        goodMatchServiceFuture flatMap { goodMatchService =>
          handleLine(line, goodMatchService, db)
        }
      }
      val upsertResultsFuture = Future.sequence(futureUpsertResults)
      val upsertResults = scala.concurrent.Await.result(upsertResultsFuture, 5 seconds)

      val stats = calculateStats(upsertResults)

      println(s"+=+=+=+=+=+=+=+=+=+=+=+=+=+=+= stats: +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=\n $stats")
    }
  }


  private def calculateStats(upsertResults: Iterator[SalesUpsertResult]): ResultStats = {
    var parseError = 0
    var saleExists = 0
    var newInn = 0
    var newShop = 0
    var goodMatchingResultChanged = 0

    var inserted = 0
    var updated = 0
    var didNothing = 0

    def handleStatsInfo(info: InfoForStats): Unit = {
      if (info.parseError) {
        parseError += 1
      }
      if (info.saleExists) {
        saleExists += 1
      }
      if (info.newInn) {
        newInn += 1
      }
      if (info.newShop) {
        newShop += 1
      }
      if (info.goodMatchingResultChanged) {
        goodMatchingResultChanged += 1
      }
    }

    upsertResults foreach {
      case Inserted(info) =>
        inserted += 1
        handleStatsInfo(info)
      case Updated(count, info) =>
        updated += 1
        handleStatsInfo(info)
      case DidNothing(info) =>
        didNothing += 1
        handleStatsInfo(info)
    }

    ResultStats(
      PerformedActionStats(
        inserted,
        updated,
        didNothing),
      parseError,
      saleExists,
      newInn,
      newShop,
      goodMatchingResultChanged)
  }

  def handleLine(line: String, goodMatchService: GoodMatchService, db: Database): Future[SalesUpsertResult] = line match {
    case pattern(orgName,orgInn,rawAddress,operationType,reqDocDateTime,rqId,ean13,productName,quantity,rawPrice,paymentType) =>
      val address = normalizedAddress(rawAddress)
      val price = (rawPrice.toDouble * 100).toLong
      val goodFoundByName = goodMatchService.find(productName)
      val goodId = goodFoundByName.map(_.id.get)
      //println(s"~~~ orgName: $orgName\norgInn: $orgInn\naddress: $address\noperationType: $operationType\nproductName: $productName\nprice:$price,\npaymentType: $paymentType")
      val saleFromLine = Sale(None,orgName,orgInn.toLong,address,operationType,asDateTime(reqDocDateTime),rqId.toLong,ean13,productName,quantity.toInt,price,paymentType,goodId)

      def doUpsertSale(info: InfoForStats): DBIO[SalesUpsertResult] = for {
        rowsAffected <- sales.filter { s =>
          s.orgInn === orgInn.toLong &&
            s.reqDocDateTime === asDateTime(reqDocDateTime) &&
            s.rqId === rqId.toLong
        }.map(_.withoutId).update(saleFromLine.withoutId)

        upsertResult <- rowsAffected match {
          case 0 => (sales += saleFromLine).map(_ => Inserted(info))
          case n => DBIO.successful(Updated(n, info))
        }
      } yield upsertResult

      def checkInnAndThen(doUpsert: InfoForStats => DBIO[SalesUpsertResult])(info: InfoForStats): DBIO[SalesUpsertResult] = for {
        innExists <- sales.filter(_.orgInn === orgInn.toLong).exists.result
        result <- {
          if (innExists) {
            println(s"----------------------inn exists: $orgInn")
            doUpsert(info)
          } else {
            doUpsert(info.copy(newInn = true))
          }
        }

      } yield result

      def checkAddressAndThen(doUpsert: InfoForStats => DBIO[SalesUpsertResult])(info: InfoForStats): DBIO[SalesUpsertResult] = {
        for {
          a1 <- sql"select count(*) from shop where inn = ${orgInn.toLong} and addr_hash = $address".as[Int].head
          u <- {
            if (a1 > 0) {
              println(s"======================= addr exists: $address")
              doUpsert(info)
            } else {
              println(s"======================= addr does not exist in db:$address")
              val insertShop = ShopDAO.shops += Shop(None, address, orgInn.toLong, level = 3, address)
              insertShop andThen doUpsert(info.copy(newShop = true))
            }
          }
        } yield u
      }

      val upsertSale: DBIO[SalesUpsertResult] = for {

        exists <- sales.filter { sale =>
          sale.orgInn === orgInn.toLong &&
            sale.reqDocDateTime === asDateTime(reqDocDateTime) &&
            sale.rqId === rqId.toLong &&
            sale.productName === productName
        } .exists.result

        result <- {

          val upsertIt =
            checkInnAndThen(
              checkAddressAndThen(
                doUpsertSale)) _

          val info = InfoForStats()

          if (exists) {
            println(s"~~~ exists: inn: $orgInn, reqDocDateTime: $reqDocDateTime")
            upsertIt(info.copy(saleExists = true))
          } else {
            upsertIt(info)
          }

        }

      } yield result

      val upsertedSale = db.run(upsertSale)
      upsertedSale onComplete { r =>
        println(s"=-=-=-=- futureInsertedSale result: $r")
      }
      upsertedSale
    case _ =>
      println(s"cannot parse line: $line")
      Future.successful(DidNothing(InfoForStats(parseError = true)))
  }

  def asDateTime(str: String): DateTime = {
    val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.S")
    formatter.parseDateTime(str)
  }

  def normalizedAddress(address: String): String = {
    import java.util.regex.Pattern
    val pattern = Pattern.compile("[^\\w\\d]", Pattern.UNICODE_CHARACTER_CLASS)
    pattern.matcher(address).replaceAll(" ")
      .toLowerCase
      .replaceAll("\\s{2,}", " ")
      .replaceAllLiterally("г москва", "")
      .replaceAllLiterally("ул", "")
      .replaceAllLiterally("улица", "")
      .replaceFirst("\\d{6}", "")
  }

}

object Schemas {

  case class Sale(
    id: Option[Long],
    orgName: String,
    orgInn: Long,
    address: String,
    operationType: String,
    reqDocDateTime: DateTime,
    rqId: Long,
    ean13: String,
    productName: String,
    quantity: Int,
    price: Long,
    paymentType: String,
    goodId: Option[Long]
    ) {

    def withoutId = (
      orgName,
      orgInn,
      address,
      operationType,
      reqDocDateTime,
      rqId,
      ean13,
      productName,
      quantity,
      price,
      paymentType,
      goodId
    )

  }

  class Sales(tag: Tag) extends Table[Sale](tag, "sales") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def orgName = column[String]("org_name")
    def orgInn = column[Long]("org_inn")
    def address = column[String]("address")
    def operationType = column[String]("operation_type")
    def reqDocDateTime = column[DateTime]("req_doc_date_time")
    def rqId = column[Long]("rq_id")
    def ean13 = column[String]("ean13")
    def productName = column[String]("product_name")
    def quantity = column[Int]("quantity")
    def price = column[Long]("price")
    def paymentType = column[String]("payment_type")
    def goodId = column[Option[Long]]("good_id")

    def withoutId = (
      orgName,
      orgInn,
      address,
      operationType,
      reqDocDateTime,
      rqId,
      ean13,
      productName,
      quantity,
      price,
      paymentType,
      goodId
    )

    def * = (
      id,
      orgName,
      orgInn,
      address,
      operationType,
      reqDocDateTime,
      rqId,
      ean13,
      productName,
      quantity,
      price,
      paymentType,
      goodId
    ).mapTo[Sale]
  }

  val sales = TableQuery[Sales]
}

object Utils {

  case class PerformedActionStats(inserted: Int,
                                  updated: Int,
                                  didNothing: Int)

  case class ResultStats(performedActionStats: PerformedActionStats,
                         parseError: Int,
                         saleExists: Int,
                         newInn: Int,
                         newShop: Int,
                         goodMatchingResultChanged: Int)

  // stats:
  // parse error
  // sale exists
  // new inn
  // new shop
  // good matching result changed

  case class InfoForStats(parseError: Boolean = false,
                          saleExists: Boolean = false,
                          newInn: Boolean = false,
                          newShop: Boolean = false,
                          goodMatchingResultChanged: Boolean = false)

  sealed trait SalesUpsertResult

  case class Inserted(info: InfoForStats = InfoForStats()) extends SalesUpsertResult
  case class Updated(count: Int, info: InfoForStats = InfoForStats()) extends SalesUpsertResult
  case class DidNothing(info: InfoForStats) extends SalesUpsertResult

  def using[A <: AutoCloseable, B](resource: A)(block: A => B): B =
    try block(resource) finally resource.close()
}
