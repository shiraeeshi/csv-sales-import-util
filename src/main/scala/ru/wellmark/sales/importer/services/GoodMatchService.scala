package ru.wellmark.sales.importer.services

import org.slf4j.{Logger, LoggerFactory}
import ru.wellmark.sales.importer.data.GoodsEntry

import scala.util.{Failure, Success, Try}

trait GoodMatcher {
  def load(seq: Seq[(Long, String)]): Unit
  def find(name: String): (String, CLemmasStats)
}

trait GoodMatchService {
  val matcher: GoodMatcher
  val goodById: Map[Long, GoodsEntry]

  val log: Logger = LoggerFactory.getLogger("gms")

  private lazy val init: Unit = matcher.load(goodById.values.map(x => x.id.get -> x.title).toSeq)

  def find(name: String): Option[GoodsEntry] = {
    init

    Try(matcher.find(name)) match {
      case Success((code, i)) if code.nonEmpty && i.m_fSumWeight >= 1.0 =>
        log.trace(s"Matched `$name` with code:`$code` and weight:`${i.m_fSumWeight}`")
        Option(goodById(code.toLong))
      case Success((code, i)) =>
        log.trace(s"Matched `$name` with code:`$code` and weight:`${i.m_fSumWeight}`")
        None
      case Failure(e) =>
        log.trace(s"NOT Matched `$name` with exception:`${e.getMessage}`")
        None
    }
  }
}
