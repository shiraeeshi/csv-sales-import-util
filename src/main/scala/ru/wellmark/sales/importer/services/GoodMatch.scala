package ru.wellmark.sales.importer.services

import scala.collection.mutable
import scala.language._
import scala.util.matching.Regex
import scala.io._
//import scala.sys.process._
//import scala.sys.process.processInternal.URL
/*
class CSVReader(){//inputStream: InputStream, bufferSize: Int)(implicit val codec: Codec) extends Source {
    //def this(inputStream: InputStream)(implicit codec: Codec) = this(inputStream, DefaultBufSize)(codec)
    //def reader() = new InputStreamReader(inputStream, codec.decoder)
  protected var filename: String
  def CSVReader(filenam: String) = {
    filename = filenam
  }
  //def bufferedReader() = new BufferedReader(reader(), bufferSize)

  // The same reader has to be shared between the iterators produced
  // by iter and getLines. This is because calling hasNext can cause a
  // block of data to be read from the stream, which will then be lost
  // to getLines if it creates a new reader, even though next() was
  // never called on the original.
  private var charReaderCreated = false
  private lazy val charReader = {
    charReaderCreated = true
    //bufferedReader()
    val source = io.Source.fromFile(filename)
    source.getLines
  }

  override lazy val iter = (
    Iterator
      continually (codec wrap charReader.read())
      takeWhile (_ != -1)
      map (_.toChar)
    )

  private def decachedReader: BufferedReader = {
    // Don't want to lose a buffered char sitting in iter either. Yes,
    // this is ridiculous, but if I can't get rid of Source, and all the
    // Iterator bits are designed into Source, and people create Sources
    // in the repl, and the repl calls toString for the result line, and
    // that calls hasNext to find out if they're empty, and that leads
    // to chars being buffered, and no, I don't work here, they left a
    // door unlocked.
    // To avoid inflicting this silliness indiscriminately, we can
    // skip it if the char reader was never created: and almost always
    // it will not have been created, since getLines will be called
    // immediately on the source.
    if (charReaderCreated && iter.hasNext) {
      val pb = new PushbackReader(charReader)
      pb unread iter.next().toInt
      new BufferedReader(pb, bufferSize)
    }
    else charReader
  }


  class ParsedFieldsIterator extends AbstractIterator[Array[String]] with Iterator[Array[String]] {
    private val lineReader = decachedReader
    var nextLine: String = null

    override def hasNext = {
      if (nextLine == null)
        nextLine = lineReader.readLine

      nextLine != null
    }
    override def next(): String = {
      val result = {
        if (nextLine == null) lineReader.readLine
        else try nextLine finally nextLine = null
      }
      if (result == null) Iterator.empty.next()
      else result
    }
  }
  //def CSVReader(String:)
  def iterateFile(filename: String): Iterator[Array[String]] = {
    val source = io.Source.fromFile(filename)
    new ParsedFieldsIterator(source.getLines)
  }

  def iterateURL(url: String): Iterator[Array[String]]
  def getFields: Iterator[Array[String]]  //  = new BufferedLineIterator
}
trait Lemmatizer {

}
 */

//trait FileLogger extends Logger
/*
val out = new PrintWriter("numbers.txt")
for (i <- 1 to 100) out.println(i)
out.print("%6d %10.2f".format(q,p))
out.close()
 */

import java.nio.charset.CodingErrorAction

import scala.io.Codec

class CLemma(plemma: String, pcode: String, pindex: Int, plemcount: Int) extends Ordered[CLemma] {

  var m_lemma: String = plemma // lemma string
  var m_code: String = pcode // parent product code (for ex. EAN13)
  var m_index: Int = pindex // lemma index in parent product's name
  var m_lemcount: Int = plemcount // lemmas count in parent product

  override def toString = m_lemma + pindex.toString() + m_code
  // return 0 if the same, negative if this < that, positive if this > that
  def compare(that: CLemma) = {
    this.toString().compare(that.toString())

  }

}

class CLemmasStats(plemcount: Int = 0, pBitmask: Int = 0, pWeight: Float = 0) {
  var m_lemcount: Int = plemcount
  var m_bmOverlappedLemmas: Int = pBitmask
  var m_fSumWeight: Float = pWeight
}

class CGoodMatcher() extends GoodMatcher {

  var mapDictByCode = new scala.collection.mutable.TreeMap[String, String]
  var mapLemmas = scala.collection.mutable.SortedSet[CLemma]() // No implicit Ordering defined for CLemma
  //    val mapLemmas = scala.collection.mutable.SortedSet[CLemma]()

  def remove_specials(pname: String): String = {
    // remove special characters
    var name_field = pname
      .replaceAll("\\*", " ")
      .replaceAll("!", " ")
      .replaceAll("\"", " ")
      .replaceAll("""\[""", " ")
      .replaceAll("""\]""", " ")
      .replaceAll("  ", " ")
      .replaceAll("  ", " ")
      .replaceAll("  ", " ")
    if (name_field.charAt(0) == ' ') name_field = name_field.drop(1)
    name_field
  }

  def preprocess_name(name: String): String = {
    var nname = name.replace(""" ПЛЕН ОБ """, " ПЛЕН/ОБ ")

    // Prefixes like 1/1 or Code
    var r = new Regex(raw"^([\d+|\d+\/\d+])+\s(.*)")
    var mi = r.findAllIn(nname)
    if (mi.hasNext) {
      //val d = mi.next
      nname = mi.group(2)
//      println("!!!!!!!!!! " + nname.toString)
    }

    // Postfixes
    r = new Regex(raw"(.*)([\s,\.\(]+СО СКИД[К|А|ОЙ|И]*[\s,\.]+)")
    mi = r.findAllIn(nname)
    if (mi.hasNext) {
      //val d = mi.next
      nname = mi.group(1)
    }

    // TODO:  2,2 МЛ -> 2,2МЛ
    r = new Regex(raw"(?!\w)\d+ (ГР|МГ|МЛ|ММ|Г|ГР|Л|СМ|ШТ|УП)[\s,\.]+")
    mi = r.findAllIn(nname)
    while (mi.hasNext) {
      val d = mi.next
      nname = nname.replace(" " + mi.group(1), mi.group(1)) // REDO: возможна некорректная замена
    }

    r = new Regex(raw"([\s,\.]\d+) ТАБ[\s,\.]+")
    mi = r.findAllIn(nname)
    while (mi.hasNext) {
      val d = mi.next
      nname = nname.replace(mi.group(1) + " ТАБ", " N" + new String(mi.group(1)).drop(1) + " ТАБ") // REDO: возможна некорректная замена
    }
    r = new Regex(raw"(\s*№\s*)\d*")
    mi = r.findAllIn(nname)
    while (mi.hasNext) {
      val d = mi.next
      nname = nname.replace(mi.group(1), " N") // REDO: возможна некорректная замена
    }
    r = new Regex(raw"([\s,.-][XХ]\s*)\d*")
    mi = r.findAllIn(nname)
    while (mi.hasNext) {
      val d = mi.next
      nname = nname.replace(mi.group(1), " N") // REDO: возможна некорректная замена
    }

    r = new Regex(raw"[\s.,](Д/)(?!\s)") // Д/ГОРЛА
    mi = r.findAllIn(nname)
    while (mi.hasNext) {
      val d = mi.next
      nname = nname.replace(mi.group(1), " ДЛЯ ")
    }

    // TODO: определять включения англ.букв в русские слова и заменять k,a,o,h,c,t,e,x
    nname = nname.replaceAll("K", "К")
    nname = nname.replaceAll("A", "А")
    nname = nname.replaceAll("O", "О")
    nname = nname.replaceAll("H", "Н")
    nname = nname.replaceAll("C", "С")
    nname = nname.replaceAll("T", "Т")
    nname = nname.replaceAll("E", "Е")
    nname = nname.replaceAll("X", "Х")

    nname = nname.replace("БАKТИСТАТИН", "БАКТИСТАТИН")

    nname = nname.replace("ЛАВОМАKС", "ЛАВОМАКС")
    nname = nname.replace("АKВАЛОР", "АКВАЛОР")

    // аквалор fix
    nname = nname.replace("KАЛЬЦИЙ", "КАЛЬЦИЙ")
    nname = nname.replace("КАЛЬЦИЙ Д3", "КАЛЬЦИЙ-Д3")
    nname = nname.replace("НИKОМЕД", "НИКОМЕД")
    nname = nname.replace("Д3-НИКОМЕД", "Д3 НИКОМЕД")
    nname = nname.replace("ХИЛАK", "ХИЛАК")
    nname = nname.replace("ХИЛАК-ФОРТЕ", "ХИЛАК ФОРТЕ")
    nname = nname.replace("1,1МЛ", "1-1МЛ")
    nname = nname.replace("1.1МЛ", "1-1МЛ")
    nname = nname.replace("2,2МЛ", "2-2МЛ")
    nname = nname.replace("2.2МЛ", "2-2МЛ")

    nname = nname.replace("KЛУБНИКА", "КЛУБНИКА")
    nname = nname.replace("KАПЛИ", "КАПЛИ")

    nname = nname.replace("БАKТИСТАТИН", "БАКТИСТАТИН")
    nname = nname.replace("ЛАВОМАKС", "ЛАВОМАКС")
    nname = nname.replace("АKВАЛОР", "АКВАЛОР")
    nname = nname.replace("БЭБИ", "БЕБИ")
    nname = nname.replace("BABY", "БЕБИ")
    nname = nname.replace("SOFT", "СОФТ")
    nname = nname.replace("FORTE", "ФОРТЕ")
//if (nname.contains("2МЛ")) println(nname)

    // TODO:   таб.п.о.   in   лавомакс 125 мг 10 таб.п.о.
    val lemmas = nname.split(" ")
    var l: String = ""
    nname = ""
    for (l0 <- lemmas) {
      l = l0
      // preprocessing for PHARMA products
      if (l.takeRight(1) == ".") l = l.dropRight(1)
      l = l match {
        case "Р-Р"  => "РАСТВОР"
        case "Р-РА" => "РАСТВОР"
        case "ТАБ"  => "ТАБЛЕТКИ"
        case "ТБЛ"  => "ТАБЛЕТКИ"
        case "ТБ"   => "ТАБЛЕТКИ"
        case "ВТФ"  => "ТАБЛЕТКИ"
        case "АМП"  => "АМПУЛЫ"
        case "ФЛ"   => "ФЛАКОН"
        case "БЛ"   => "БЛИСТЕР"
        case "УП"   => ""
        case "П/О"  => "ПЛЕН/ОБ"
        case "П.О"  => "ПЛЕН/ОБ"
        case "ВН"   => "ВНУТР"
        case "КАПС" => "КАПСУЛЫ"
        //l.replace("ГЛАЗН")
        // НАРУЖН
        // ТУБА
        // ДРАЖЕ
        // МАЗЬ
        // ФЛ-КАП
        // ФИЛЬТР-ПАКЕТ
        // Д/И    -- для иньекций
        case "\"" => " "
        case _    => l
      }
      //      print(l + " ")
      nname += l + " "
    }
//println(nname)
    nname.trim()

  }

  def load(filename: String): Unit = {

    //https://stackoverflow.com/questions/13625024/how-to-read-a-text-file-with-mixed-encodings-in-scala-or-java
    implicit val codec = Codec("UTF-8") // x-MacCyrillic")   /// CP1251")
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

    //"src/main/scala/f_npv_.csv")
    // "src/main/scala/f_nv_20151109.csv") // f_npv_.csv")
    val source = Source.fromFile(filename)
    var lineIterator = source.getLines

    var icode_field = 0
    for (l <- lineIterator) {
      icode_field += 1
      val fields = l.split(";")
      // take 1st field
      var name_field = fields(1).toUpperCase
      val code_field = icode_field.toString()

      if (!(code_field == "" || code_field == "null")) {

        name_field = remove_specials(name_field)
        name_field = preprocess_name(name_field)

        mapDictByCode += (code_field -> name_field)

        // fill lemmas
        val lemmas = name_field.split("[ ,.]") // standard split
        //        println(lemmas.length)
        var i = 0
        for (ll <- lemmas) {
          mapLemmas += new CLemma(ll, code_field, i, lemmas.length)
          i += 1
          //          println(i + " " + ll + " " + code_field + " "+i.toString())
        }

//        println(code_field + " " + name_field)
      }
    }
//    println("Total products: " + mapDictByCode.size.toString)
//    println("Total lemmas: " + mapLemmas.size.toString)
    source.close()

  }

  def load(seq: Seq[(Long, String)]): Unit = {
    for ((id, name) <- seq) {
      var name_field = name.toUpperCase
      val code_field = id.toString()
//      println(name)
      name_field = remove_specials(name_field)
      name_field = preprocess_name(name_field)
      mapDictByCode += (code_field -> name_field)
      val lemmas = name_field.split("[ ,.]") // standard split
      var i = 0
      for (ll <- lemmas) {
        mapLemmas += new CLemma(ll, code_field, i, lemmas.length)
        i += 1
      }
    }
//    println("Total products: " + mapDictByCode.size.toString)
//    println("Total lemmas: " + mapLemmas.size.toString)
  }

  def find(sLine: String): (String, CLemmasStats) = {

//    sLine = "РОТОКАН Р-Р 50МЛ"    // "ШАЛФЕЙ БРОНХОАКТИВ ТАБЛ.Д/РАССАС. N20"
//    println( "Processing: " + sLine )
    var sLineUp = remove_specials(sLine.toUpperCase)
    var sIncoming = preprocess_name(sLineUp)
//    println (":" + sIncoming )
    var entriesHash = new scala.collection.mutable.HashMap[String, CLemmasStats]

    // for each lemma
    var lemmas = sIncoming.split("[ ,.]")
    var iLemma = 0
    for (ll <- lemmas) {

      var sSearched = ll
      var lemmasFound: mutable.Set[CLemma] = scala.collection.mutable.Set[CLemma]()
      var isLemmaFound = false
      while (isLemmaFound == false) {
        //        println("Searching for: " + sSearched)
        // REDO: Use search in sorted set(!!!) O(N) -> O(log2(N))
        lemmasFound = mapLemmas.filter(l => (l.m_lemma != "" && l.m_lemma.startsWith(sSearched) == true))

        //        println("Found: " + lemmasFound.size )
        if (lemmasFound.size > 0) isLemmaFound = true
        if (isLemmaFound == false) sSearched = sSearched.dropRight(1)
        if (sSearched.length() == 0) isLemmaFound = true
      }
      // if lemma found - store it
      if (isLemmaFound && sSearched.length() > 0) {
        for (cc <- lemmasFound) {
          if (!entriesHash.contains(cc.m_code)) {
            entriesHash(cc.m_code) =
              new CLemmasStats(plemcount = cc.m_lemcount, pBitmask = (1 << cc.m_index), pWeight = 1)
          } else {
            entriesHash(cc.m_code).m_bmOverlappedLemmas |= (1 << cc.m_index)
            entriesHash(cc.m_code).m_fSumWeight += Math
              .sqrt(sSearched.length)
              .toFloat * sSearched.length / ll.length // 1
          }
          val bitCount: (Int) => Int = java.lang.Integer.bitCount
          //          println(cc.m_code + " " + cc.m_index +"/"+cc.m_lemcount+": " + entriesHash(cc.m_code).m_fSumWeight + " " + bitCount(entriesHash(cc.m_code).m_bmOverlappedLemmas))
        }
      }
      iLemma += 1
      //      println(i + " " + ll + " " + code_field + " "+i.toString())
    }
    var recalcMap = entriesHash map {
      case (code, stats) =>
        (code, new CLemmasStats(stats.m_lemcount, stats.m_bmOverlappedLemmas, stats.m_fSumWeight / stats.m_lemcount))
    }
    var maxMap = entriesHash.maxBy {
      case (code, stats) => stats.m_fSumWeight / stats.m_lemcount
    }
    //for(e <- recalcMap) {
    //println(e._1 + " " + e._2.m_fSumWeight)
    //}
//    println(maxMap._1 + " " + maxMap._2.m_fSumWeight / maxMap._2.m_lemcount)
    if (maxMap._2.m_fSumWeight / maxMap._2.m_lemcount >= 1.0f) { //  > 0.7f) {
      //      println("Found!")
      maxMap
    } else {
//      println("Not found '" + sLine +"' (" + maxMap._1 + ")")
      ("", new CLemmasStats())

    }
  }
}

class CGoodMatcherFRD() extends GoodMatcher {

  var mapDictByCode = new scala.collection.mutable.TreeMap[String, String]
  var mapLemmas = scala.collection.mutable.SortedSet[CLemma]() // No implicit Ordering defined for CLemma
  //    val mapLemmas = scala.collection.mutable.SortedSet[CLemma]()

  def remove_specials(pname: String): String = {
    // remove special characters
    var name_field = pname
      .replaceAll("\\*", " ")
      .replaceAll("!", " ")
      .replaceAll("\"", " ")
      .replaceAll("""\[""", " ")
      .replaceAll("""\]""", " ")
      .replaceAll("  ", " ")
      .replaceAll("  ", " ")
      .replaceAll("  ", " ")
    if (name_field.charAt(0) == ' ') name_field = name_field.drop(1)
    name_field
  }

  def preprocess_name(name: String): String = {
    var nname = name.replace(""" ПЛЕН ОБ """, " ПЛЕН/ОБ ")

    // Prefixes like 1/1 or Code
    var r = new Regex(raw"^([\d+|\d+\/\d+])+\s(.*)")
    var mi = r.findAllIn(nname)
    if (mi.hasNext) {
      //val d = mi.next
      nname = mi.group(2)
      //      println("!!!!!!!!!! " + nname.toString)
    }

    // Postfixes
    r = new Regex(raw"(.*)([\s,\.\(]+СО СКИД[К|А|ОЙ|И]*[\s,\.]+)")
    mi = r.findAllIn(nname)
    if (mi.hasNext) {
      //val d = mi.next
      nname = mi.group(1)
    }

    // TODO:  2,2 МЛ -> 2,2МЛ
    r = new Regex(raw"(?!\w)\d+ (ГР|МГ|МЛ|ММ|Г|ГР|Л|СМ|ШТ|УП)[\s,\.]+")
    mi = r.findAllIn(nname)
    while (mi.hasNext) {
      val d = mi.next
      nname = nname.replace(" " + mi.group(1), mi.group(1)) // REDO: возможна некорректная замена
    }

    r = new Regex(raw"([\s,\.]\d+) ТАБ[\s,\.]+")
    mi = r.findAllIn(nname)
    while (mi.hasNext) {
      val d = mi.next
      nname = nname.replace(mi.group(1) + " ТАБ", " N" + new String(mi.group(1)).drop(1) + " ТАБ") // REDO: возможна некорректная замена
    }
    r = new Regex(raw"(\s*№\s*)\d*")
    mi = r.findAllIn(nname)
    while (mi.hasNext) {
      val d = mi.next
      nname = nname.replace(mi.group(1), " N") // REDO: возможна некорректная замена
    }
    r = new Regex(raw"([\s,.-][XХ]\s*)\d*")
    mi = r.findAllIn(nname)
    while (mi.hasNext) {
      val d = mi.next
      nname = nname.replace(mi.group(1), " N") // REDO: возможна некорректная замена
    }

    r = new Regex(raw"[\s.,](Д/)(?!\s)") // Д/ГОРЛА
    mi = r.findAllIn(nname)
    while (mi.hasNext) {
      val d = mi.next
      nname = nname.replace(mi.group(1), " ДЛЯ ")
    }

    // TODO: определять включения англ.букв в русские слова и заменять k,a,o,h,c,t,e,x
    nname = nname.replaceAll("K", "К")
    nname = nname.replaceAll("A", "А")
    nname = nname.replaceAll("O", "О")
    nname = nname.replaceAll("H", "Н")
    nname = nname.replaceAll("C", "С")
    nname = nname.replaceAll("T", "Т")
    nname = nname.replaceAll("E", "Е")
    nname = nname.replaceAll("X", "Х")

    nname = nname.replace("БАKТИСТАТИН", "БАКТИСТАТИН")

    nname = nname.replace("ЛАВОМАKС", "ЛАВОМАКС")
    nname = nname.replace("АKВАЛОР", "АКВАЛОР")

    // аквалор fix
    nname = nname.replace("KАЛЬЦИЙ", "КАЛЬЦИЙ")
    nname = nname.replace("КАЛЬЦИЙ Д3", "КАЛЬЦИЙ-Д3")
    nname = nname.replace("НИKОМЕД", "НИКОМЕД")
    nname = nname.replace("Д3-НИКОМЕД", "Д3 НИКОМЕД")
    nname = nname.replace("ХИЛАK", "ХИЛАК")
    nname = nname.replace("ХИЛАК-ФОРТЕ", "ХИЛАК ФОРТЕ")
    nname = nname.replace("1,1МЛ", "1-1МЛ")
    nname = nname.replace("1.1МЛ", "1-1МЛ")
    nname = nname.replace("2,2МЛ", "2-2МЛ")
    nname = nname.replace("2.2МЛ", "2-2МЛ")

    nname = nname.replace("KЛУБНИКА", "КЛУБНИКА")
    nname = nname.replace("KАПЛИ", "КАПЛИ")

    nname = nname.replace("БАKТИСТАТИН", "БАКТИСТАТИН")
    nname = nname.replace("ЛАВОМАKС", "ЛАВОМАКС")
    nname = nname.replace("АKВАЛОР", "АКВАЛОР")
    nname = nname.replace("БЭБИ", "БЕБИ")
    nname = nname.replace("BABY", "БЕБИ")
    nname = nname.replace("SOFT", "СОФТ")
    nname = nname.replace("FORTE", "ФОРТЕ")
    //if (nname.contains("2МЛ")) println(nname)

    // TODO:   таб.п.о.   in   лавомакс 125 мг 10 таб.п.о.
    val lemmas = nname.split(" ")
    var l: String = ""
    nname = ""
    for (l0 <- lemmas) {
      l = l0
      // preprocessing for PHARMA products
      if (l.takeRight(1) == ".") l = l.dropRight(1)
      l = l match {
        case "Р-Р"  => "РАСТВОР"
        case "Р-РА" => "РАСТВОР"
        case "ТАБ"  => "ТАБЛЕТКИ"
        case "ТБЛ"  => "ТАБЛЕТКИ"
        case "ТБ"   => "ТАБЛЕТКИ"
        case "ВТФ"  => "ТАБЛЕТКИ"
        case "АМП"  => "АМПУЛЫ"
        case "ФЛ"   => "ФЛАКОН"
        case "БЛ"   => "БЛИСТЕР"
        case "УП"   => ""
        case "П/О"  => "ПЛЕН/ОБ"
        case "П.О"  => "ПЛЕН/ОБ"
        case "ВН"   => "ВНУТР"
        case "КАПС" => "КАПСУЛЫ"
        //l.replace("ГЛАЗН")
        // НАРУЖН
        // ТУБА
        // ДРАЖЕ
        // МАЗЬ
        // ФЛ-КАП
        // ФИЛЬТР-ПАКЕТ
        // Д/И    -- для иньекций
        case "\"" => " "
        case _    => l
      }
      //      print(l + " ")
      nname += l + " "
    }
    //println(nname)
    nname.trim()

  }

  def load(filename: String): Unit = {

    //https://stackoverflow.com/questions/13625024/how-to-read-a-text-file-with-mixed-encodings-in-scala-or-java
    implicit val codec = Codec("UTF-8") // x-MacCyrillic")   /// CP1251")
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

    //"src/main/scala/f_npv_.csv")
    // "src/main/scala/f_nv_20151109.csv") // f_npv_.csv")
    val source = Source.fromFile(filename)
    var lineIterator = source.getLines

    var icode_field = 0
    for (l <- lineIterator) {
      icode_field += 1
      val fields = l.split(";")
      // take 1st field
      var name_field = fields(1).toUpperCase
      val code_field = icode_field.toString()

      if (!(code_field == "" || code_field == "null")) {

        name_field = remove_specials(name_field)
        name_field = preprocess_name(name_field)

        mapDictByCode += (code_field -> name_field)

        // fill lemmas
        val lemmas = name_field.split("[ ,.]") // standard split
        //        println(lemmas.length)
        var i = 0
        for (ll <- lemmas) {
          mapLemmas += new CLemma(ll, code_field, i, lemmas.length)
          i += 1
          //          println(i + " " + ll + " " + code_field + " "+i.toString())
        }

        //        println(code_field + " " + name_field)
      }
    }
    //    println("Total products: " + mapDictByCode.size.toString)
    //    println("Total lemmas: " + mapLemmas.size.toString)
    source.close()

  }

  def load(seq: Seq[(Long, String)]): Unit = {
    for ((id, name) <- seq) {
      var name_field = name.toUpperCase
      val code_field = id.toString()
      //      println(name)
      name_field = remove_specials(name_field)
      name_field = preprocess_name(name_field)
      mapDictByCode += (code_field -> name_field)
      val lemmas = name_field.split("[ ,.]") // standard split
      var i = 0
      for (ll <- lemmas) {
        mapLemmas += new CLemma(ll, code_field, i, lemmas.length)
        i += 1
      }
    }
    //    println("Total products: " + mapDictByCode.size.toString)
    //    println("Total lemmas: " + mapLemmas.size.toString)
  }

  def find(sLine: String): (String, CLemmasStats) = {

    //    sLine = "РОТОКАН Р-Р 50МЛ"    // "ШАЛФЕЙ БРОНХОАКТИВ ТАБЛ.Д/РАССАС. N20"
    //    println( "Processing: " + sLine )
    var sLineUp = remove_specials(sLine.toUpperCase)
    var sIncoming = preprocess_name(sLineUp)
    //    println (":" + sIncoming )
    var entriesHash = new scala.collection.mutable.HashMap[String, CLemmasStats]

    // for each lemma
    var lemmas = sIncoming.split("[ ,.]")
    var iLemma = 0
    for (ll <- lemmas) {

      var sSearched = ll
      var lemmasFound: mutable.Set[CLemma] = scala.collection.mutable.Set[CLemma]()
      var isLemmaFound = false
      while (!isLemmaFound) {
        //        println("Searching for: " + sSearched)
        // REDO: Use search in sorted set(!!!) O(N) -> O(log2(N))
        lemmasFound = mapLemmas.filter(
          l =>
            l.m_lemma != "" && l.m_lemma.startsWith(sSearched) &&
              (l.m_index > 0 || l.m_lemma == sSearched && sSearched == ll) // доп.условие для Exact match 1го компонента названия товара из mapLemmas (словаря лемм всех загруженных товаров)
        )
        //        println("Found: " + lemmasFound.size )
        if (lemmasFound.nonEmpty) isLemmaFound = true
        if (!isLemmaFound) sSearched = sSearched.dropRight(1)
        if (sSearched.length() == 0) isLemmaFound = true
      }
      // if lemma found - store it
      if (isLemmaFound && sSearched.length() > 0) {
        for (cc <- lemmasFound) {
          if (!entriesHash.contains(cc.m_code)) {
            entriesHash(cc.m_code) =
              new CLemmasStats(plemcount = cc.m_lemcount, pBitmask = 1 << cc.m_index, pWeight = 1)
          } else {
            if ((entriesHash(cc.m_code).m_bmOverlappedLemmas & (1 << cc.m_index))==0) {
              entriesHash(cc.m_code).m_bmOverlappedLemmas |= (1 << cc.m_index)
              entriesHash(cc.m_code).m_fSumWeight += Math
                .sqrt(sSearched.length)
                .toFloat * sSearched.length / cc.m_lemma.length // ll.length // 1
            }
          }
          val bitCount: (Int) => Int = java.lang.Integer.bitCount
          //          println(cc.m_code + " " + cc.m_index +"/"+cc.m_lemcount+": " + entriesHash(cc.m_code).m_fSumWeight + " " + bitCount(entriesHash(cc.m_code).m_bmOverlappedLemmas))
        }
      }
      iLemma += 1
      //      println(i + " " + ll + " " + code_field + " "+i.toString())
    }
    var recalcMap = entriesHash map {
      case (code, stats) =>
        (code, new CLemmasStats(stats.m_lemcount, stats.m_bmOverlappedLemmas, stats.m_fSumWeight / stats.m_lemcount))
    }
    var maxMap = entriesHash.maxBy {
      case (code, stats) => stats.m_fSumWeight / stats.m_lemcount
    }
    //for(e <- recalcMap) {
    //println(e._1 + " " + e._2.m_fSumWeight)
    //}
    //    println(maxMap._1 + " " + maxMap._2.m_fSumWeight / maxMap._2.m_lemcount)
    if (maxMap._2.m_fSumWeight / maxMap._2.m_lemcount >= 1.0f) { //  > 0.7f) {
      //      println("Found!")
      maxMap
    } else {
      //      println("Not found '" + sLine +"' (" + maxMap._1 + ")")
      ("", new CLemmasStats())

    }
  }
}
