package ru.wellmark.sales.importer.data

import slick.jdbc.PostgresProfile.api._

object GoodsEntryDAO {

  class GoodsTable(tag: Tag) extends Table[GoodsEntry](tag, "goods") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def title = column[String]("title")
    def inn = column[Long]("inn")

    def * = (id.?, title, inn).mapTo[GoodsEntry]
  }

  val goods = TableQuery[GoodsTable]

  def findByInn(inn: Long) = goods.filter(_.inn === inn).result

  def findByInns(inns: Seq[Long]) = goods.filter(_.inn inSet inns).result

}
