package ru.wellmark.sales.importer.data

case class GoodsEntry(id: Option[Long], title: String, inn: Long)
