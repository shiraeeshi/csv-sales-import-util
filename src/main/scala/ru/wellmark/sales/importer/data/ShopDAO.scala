package ru.wellmark.sales.importer.data

import slick.jdbc.PostgresProfile.api._

object ShopDAO {

  class ShopTable(tag: Tag) extends Table[Shop](tag, "shop") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def title =  column[String]("title")
    def inn = column[Long]("inn")
    def level = column[Int]("level")
    def addr_hash = column[String]("addr_hash")

    def * = (id.?, title, inn, level, addr_hash).mapTo[Shop]
  }

  val shops = TableQuery[ShopTable]

}
