package ru.wellmark.sales.importer.data

case class Shop(id: Option[Long], title: String, inn: Long, level: Int, addr_hash: String)
