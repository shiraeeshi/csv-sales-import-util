lazy val akkaHttpVersion = "10.0.11"
lazy val akkaVersion    = "2.5.11"

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "ru.wellmark",
      scalaVersion    := "2.12.4"
    )),
    name := "sales-import",
    libraryDependencies := Seq(
      "com.typesafe.slick" %% "slick" % "3.2.1",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.2.1",
      "org.postgresql" % "postgresql" % "42.0.0",
      "org.slf4j" % "slf4j-nop" % "1.6.4",
      "com.github.tototoshi" %% "slick-joda-mapper" % "2.3.0",
      "joda-time" % "joda-time" % "2.7",
      "org.joda" % "joda-convert" % "1.7"
    )
  )
